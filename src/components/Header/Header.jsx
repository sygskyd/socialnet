import React from "react";
import s from './Header.module.css'
import {NavLink} from "react-router-dom";


const Header = (props) => {
    return (<header className={s.header}>
        <img src='https://escharts.com/storage/app/uploads/public/5cb/74a/9f7/5cb74a9f70381705731227.png'/>

        <div className={s.loginBlock}>
            { props.isAuth
                ? <div> {props.login} -  <button onClick={props.logout}>Log out</button></div>
                : <NavLink to={'/login'}>Login</NavLink> }
        </div>
    </header>);
}

export default Header;