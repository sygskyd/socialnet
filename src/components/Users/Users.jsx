import React from "react";
import styles from "./users.module.css";
import {NavLink} from "react-router-dom";
import Paginator from "../common/Paginator/Paginator";
import User from "./User";


let Users = ({currentPage, totalUsersCount, pageSize, onPageChanged, setCurrentPage, ...props}) => {


    return <div>
        <Paginator currentPage={currentPage} onPageChanged={onPageChanged} totalUsersCount={totalUsersCount} pageSize={pageSize} />
        <div>
        {
            props.users.map(u => <User user={u}
                                        followingInProgress={props.followingInProgress}
                                        key={u.id}
                                        unfollow={props.unfollow}
                                        follow={props.follow} />
                                        )
        }
        </div>
         </div>
}
export default Users;