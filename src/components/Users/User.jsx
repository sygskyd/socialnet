import React from "react";
import styles from "./users.module.css";
import {NavLink} from "react-router-dom";


let User = ({user, followingInProgress, unfollow, follow}) => {
    return (
        <div>
                <span>
                    <div>
                        <NavLink to={"/profile/" + user.id}>
                        <img
                            src={user.photos.small != null ? user.photos.small : "https://yt3.ggpht.com/a/AGF-l78m2sessocMPEItrNu1UOS5sB4G-facB8lNjw=s900-c-k-c0xffffffff-no-rj-mo"}
                            className={styles.userPhoto}/>
                        </NavLink>
                    </div>
                     <div>
                         {user.followed
                             ? <button disabled={followingInProgress
                                 .some(id => id === user.id)}
                                       onClick={() => {
                                           unfollow(user.id);
                                       }}>Unfollow</button>
                             : <button disabled={followingInProgress.some(id => id === user.id)}
                                       onClick={() => {
                                           follow(user.id);
                                       }}>Follow</button>}
                    </div>
                </span>
            <span>
                   <span>
                       <div>{user.name}</div>
                       <div>{user.status}</div>
                   </span>
                   <span>
                        <div>{".location.country"}</div>
                       <div>{"user.location.city"}</div>
                   </span>
            </span>
        </div>
    )
}




export default User;