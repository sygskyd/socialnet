import React from 'react';
import {Field, reduxForm} from "redux-form";
import {Input} from "../common/FormsCOntrols/FormsControls";
import {required} from "../../utils/validators/validators";
import {connect} from "react-redux";
import {login} from "../../redux/auth-reducer";
//import Redirect from "react-router-dom/es/Redirect";
import style from "./../common/FormsCOntrols/FormsControls.module.css"
import {Redirect} from "react-router-dom";

const LoginForm = ({handleSubmit, error}) => {
    return <form onSubmit={handleSubmit}>
        <div>
            <Field placeholder={"Email"}
                   validate={[required]}
                   name={"email"} component={Input}/>
        </div>
        <div>
            <Field placeholder={"Password"}
                   validate={[required]}
                   name={"password"} component={Input} type={"password"}/>
        </div>
        <div>
            <Field type={"checkbox"}
                   validate={[required]}
                   name={"rememberMe"} component={Input}/> remember me
        </div>
        {error && <div className={style.formSummaryError}>
            {error}
        </div>
        }
            <div>
                <button>Login</button>
            </div>

    </form>

}

const LoginReduxForm = reduxForm({form: "login"})(LoginForm)


const Login = (props) => {
    const onSubmit = (formData) => {
        props.login(formData.email, formData.password, formData.rememberMe)
    }

    if (props.isAuth) {
        return <Redirect to={"/profile"}/>
    }

    return <div>
        <h1>Login</h1>
        <LoginReduxForm onSubmit={onSubmit}/>
    </div>
}

const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuth
})


export default connect(mapStateToProps, {login})(Login);