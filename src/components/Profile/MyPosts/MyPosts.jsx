import React from "react";
import s from "./MyPosts.module.css";
import Post from "./Post/Post";
import {Field, reduxForm} from "redux-form";
import {maxLengthCreator, required} from "../../../utils/validators/validators";
import {Textarea} from "../../common/FormsCOntrols/FormsControls";

const maxLength10 = maxLengthCreator(10);

let AddNewPostForm = (props) => {
    return  <form onSubmit={props.handleSubmit}>
        <div>
            <Field component={Textarea} name="newPostText" validate={[required, maxLength10]}/>
        </div>
        <div>
            <button>Add post</button>
        </div>
    </form>

}

const MyPosts = React.memo(props => {

    let postsElements =
        [...props.posts]
            .map( p =>  <Post message={p.message} likesCount={p.likesCount}/>)


    let addNewPost = (values) => {
        props.addPost(values.newPostText);
    }

    return <div className={s.postsBlock}>
        <h1>My posts</h1>
        <AddNewPostFormRedux onSubmit={addNewPost}/>
        <div className={s.posts}>
           { postsElements }
        </div>
    </div>
});




const AddNewPostFormRedux = reduxForm({form: "ProfileAddNewPostForm"})(AddNewPostForm)

export default MyPosts;