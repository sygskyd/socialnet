import React from "react";
import s from "./Post.module.css"

const Post = (props) => {


    return  (<div className={s.item}>
        <img src="https://avatars.mds.yandex.net/get-pdb/2074020/68ce6b91-e331-4494-bac8-76bd0bade173/s1200?webp=false">
        </img>
        {props.message}
        <div>
        <span>
            {props.likesCount}
        </span>
        </div>
    </div>


    );
}

export default Post;