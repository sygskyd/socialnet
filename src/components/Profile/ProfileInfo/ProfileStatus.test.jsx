import React from "react";
import { create } from "react-test-renderer";
import ProfileStatus from "./ProfileStatus";

describe("ProfileStatus component", () => {
    test("status from props should be in the state", () => {
        const component = create(<ProfileStatus status="React.memo"/>);
        const instance = component.getInstance();
        expect(instance.state.status).toBe("React.memo");
    });

    test("<span> should be with status", () => {
        const component = create(<ProfileStatus status="React.memo"/>);
        const root = component.root;
        let span = root.findByType("span")
        expect(span).not.toBeNull();
        expect(span.children[0]).toBe("React.memo");
    });

    test("status should be editMode after DoubleClick", () => {
        const component = create(<ProfileStatus status="React.memo" />);
        const root = component.root;
        let span = root.findByType("span");
        span.props.onDoubleClick();
        let input = root.findByType("input");
        expect(input.props.value).toBe("React.memo");
    });

    test("correct callbackFunction call", () => {
        const mockCallback = jest.fn();
        const component = create(<ProfileStatus status="lol" updateStatus={mockCallback} />);
        const instance = component.getInstance();
        instance.deactivateEditMode();
        expect(mockCallback.mock.calls.length).toBe(1);
    });
});