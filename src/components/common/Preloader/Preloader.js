import styles from "./preloader.module.css";
import React from "react";

let Preloader = (props) => {
    return <div className={styles.lds_ripple}>
        <div>Wait</div>
    </div>
}

export default Preloader;