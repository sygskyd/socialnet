import React, {useState} from "react";
import styles from "./Paginator.module.css";



let Paginator = ({currentPage, totalUsersCount, pageSize, onPageChanged}) => {

    let pagesCount = Math.ceil(totalUsersCount / pageSize);

    let pages = [];
    for (let i = 1; i <= pagesCount; i++) {
        pages.push(i);
    }



    return <div>
        {currentPage > 1 &&
        <button onClick={() => {

        }}>PREV</button> }


        {pages.filter((p) => p <= currentPage + 10 && p >= currentPage - 10)
            .map(p => {
            return <div className={styles.pagination}>
                    <span className={currentPage === p && styles.selectedPage}
                          onClick={() => {
                              onPageChanged(p)
                          }}>{p}</span>
            </div>
        })}
    </div>
}
export default Paginator;