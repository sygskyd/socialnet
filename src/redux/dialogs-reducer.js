const SEND_MESSAGE = "SEND_MESSAGE";

let initialState = {

    dialogs: [
        {id: 1, name: "Pomanes"},
        {id: 2, name: "Reaper"},
        {id: 3, name: "Reaper2"},
    ],
    messages: [
        {id: 1, message: "Hi"},
        {id: 2, message: "Wassup"},
        {id: 3, message: "LOL"},
        {id: 3, message: "!"}
    ]
}

const dialogsReducer = (state = initialState, action) => {

    switch (action.type) {
        case
        SEND_MESSAGE:
            let body = action.newMessageBody;
            return {
                ...state,
                messages: [...state.messages, {id: 6, message: body}]
            };

        default:
            return state;
    }

}


export const sendMessageCreator = (newMessageBody) => ({type: SEND_MESSAGE, newMessageBody: newMessageBody})


export default dialogsReducer;