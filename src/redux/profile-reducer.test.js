import profileReducer, {addPostActionCreator, deletePost} from "./profile-reducer";
import React from "react";

let state = {
    posts: [
        {id: 1, message: "Hi!", likesCount: 10},
        {id: 2, message: "first post", likesCount: 20},
    ]
};

test('post\'s length should be +1', () => {
    let action = addPostActionCreator("test text")

    let newState = profileReducer(state, action);
    expect (newState.posts.length).toBe(3);
})


test('new post message should be \'test text\'', () => {
    let action = addPostActionCreator("test text");

    let newState = profileReducer(state, action);
    expect (newState.posts[2].message).toBe('test text');
})

test('messages length after deleting should be -1', () => {
    let action = deletePost(1);

    let newState = profileReducer(state, action);
    expect (newState.posts.length).toBe(1);
});

test('messages length after deleting should\'nt be -1', () => {
    let action = deletePost(1);

    let newState = profileReducer(state, action);
    expect (newState.posts.length).toBe(1);
});

it(`after deleting length should be the same if recieved postId is incorrect`, () => {
    // 1. test data
    let action = deletePost(100000);

    // 2. action
    let newState = profileReducer(state, action);

    // 3. expectation
    expect(newState.posts.length).toBe(2);
});

