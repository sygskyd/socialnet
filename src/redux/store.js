import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";
import {sidebarReducer} from "./sidebar-reducer";


let store = {
    _state: {
        profilePage: {
            posts: [
                {id: 1, message: "Hi!", likesCount: 10},
                {id: 2, message: "first post", likesCount: 20},
            ],
            newPostText: 'lol! Reaper!'
        },
        dialogsPage: {
            dialogs: [
                {id: 1, name: "Pomanes"},
                {id: 2, name: "Reaper"},
                {id: 3, name: "Reaper2"},
            ],
            messages: [
                {id: 1, message: "Hi"},
                {id: 2, message: "Wassup"},
                {id: 3, message: "LOL"},
                {id: 3, message: "!"}
            ],
            newMessageBody: ""
        },

        sidebar: {
            friends: [
                {id: 1, name: "Pomanes"},
                {id: 1, name: "Reaper"},
                {id: 1, name: "Reaper2"}
            ]
        }
    },
    _callSubsriber() {
        console.log("State changed")
    },

    getState() {
        return this._state;
    },
    subscribe(observer) {
        this._callSubsriber = observer;
    },

    dispatch(action) {
        this._state.profilePage = profileReducer(this._state.profilePage, action);
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
        this._state.sidebar = sidebarReducer(this._state.sidebar, action);
        this._callSubsriber(this._state);
    }
}

window.store = store;
export default store;